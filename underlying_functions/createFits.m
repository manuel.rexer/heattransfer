function [fitresult, gof] = createFits(Pe, NuReal, PeImag, NuImag_cleared)
%CREATEFITS(PE,NUREAL,PEIMAG,NUIMAG_CLEARED)
%  Create fits.
%
%  Data for 'Real' fit:
%      X Input : Pe
%      Y Output: NuReal
%  Data for 'Imag linear' fit:
%      X Input : PeImag
%      Y Output: NuImag_cleared
%  Output:
%      fitresult : a cell-array of fit objects representing the fits.
%      gof : structure array with goodness-of fit info.
%
%  See also FIT, CFIT, SFIT.

%  Auto-generated by MATLAB on 28-Apr-2024 14:56:02

%% Initialization.

% Initialize arrays to store fits and goodness-of-fit.
fitresult = cell( 2, 1 );
gof = struct( 'sse', cell( 2, 1 ), ...
    'rsquare', [], 'dfe', [], 'adjrsquare', [], 'rmse', [] );

%% Fit: 'Real'.
[xData, yData] = prepareCurveData( Pe, NuReal );

% Set up fittype and options.
ft = fittype( 'a+b*x^(c)+d*x^(e)', 'independent', 'x', 'dependent', 'y');
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Robust = 'Off';
opts.Lower = [0 -inf 0.5 -inf 0.6];
opts.Upper = [30 inf 0.5 inf 10];
opts.StartPoint = [3 0.251083857976031 0.5 0.473288848902729, 1];

% Fit model to data.
[fitresult{1}, gof(1)] = fit( xData, yData, ft, opts );

% % Plot fit with data.
% figure( 'Name', 'Real LAR' );
% h = plot( fitresult{1}, xData, yData );
% legend( h, 'NuReal vs. Pe', 'Real LAR', 'Location', 'NorthEast', 'Interpreter', 'none' );
% % Label axes
% xlabel( 'Pe', 'Interpreter', 'none' );
% ylabel( 'NuReal', 'Interpreter', 'none' );
% grid on

%% Fit: 'Imag linear'.
[xData, yData] = prepareCurveData( PeImag, NuImag_cleared );

% Set up fittype and options.
ft = fittype( 'power1' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Robust = 'Off';
opts.StartPoint = [0.0399311276029186 0.836615402356409];

% Fit model to data.
[fitresult{2}, gof(2)] = fit( xData, yData, ft, opts );

% % Plot fit with data.
% figure( 'Name', 'Imag linear' );
% h = plot( fitresult{2}, xData, yData );
% legend( h, 'NuImag_cleared vs. PeImag', 'Imag linear', 'Location', 'NorthEast', 'Interpreter', 'none' );
% % Label axes
% xlabel( 'PeImag', 'Interpreter', 'none' );
% ylabel( 'NuImag_cleared', 'Interpreter', 'none' );
% grid on


